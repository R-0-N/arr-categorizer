const mutate = ([first, ...rest]) => {
    return [...rest, first].join('');
}

const isMutation = (origin, mutation, c = 1) => {
  if (origin.length !== mutation.length) return false;
  
  const mut = mutate(mutation);
  
  if (
      mut.toLowerCase() !== origin.toLowerCase()
      && c < (mut.length - 1)
  ) {
      return isMutation(origin, mut, c + 1);
  }
  
  if (c < mut.length - 1) {
      return true;
  }
  
  return false;
};


const iterateArray = ([first, ...rest], result = []) => {
    let res = result;
    const [next, ...remainder] = [...rest];
    
    if (result.length === 0) {
        res = [first];
    }
    
    if (next === undefined) {
        return [...res];
    }
    
    if (isMutation(first, next)) {
        return iterateArray([next, ...remainder], [next, ...res]);
    } else {
        return iterateArray([first, ...remainder], [...res]);
    }
}

const pushToArray = (array, result = []) => {
    let res = [...result];
    
    if (array.length === 0) {
        return res;
    }
    
    const iterationResult = iterateArray(array);
    const remainingItemsArray = array.filter((item) => !iterationResult.includes(item));
    
    return pushToArray(remainingItemsArray, [iterationResult, ...res]);
}

module.exports = {
    pushToArray,
}