## INPUT
const cities = [
    'Kyoto',
    'Tokyo',
    'Rome',
    'London',
    'Donlon',
    'Donnlo',
]

## Output
const catCities = [
    [
        'Kyoto',
        'Tokyo',
    ],
    [
        'Rome',
    ],
    [
        'London',
        'Donlon',
    ],
    [
        'Donnlo',
    ],
]

Happy Coding ;)